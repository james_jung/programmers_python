from collections import Counter

def solution(participant, completion):

    dict_participant = Counter(participant)
    dict_completion = Counter(completion)
    diff = dict_participant - dict_completion

    return list(diff.keys())[0]




participant = ["marina", "josipa", "nikola", "vinko", "filipa"]
completion = ["josipa", "filipa", "marina", "nikola"]

print(solution(participant, completion))

