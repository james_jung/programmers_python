
def solution(participant: list, completion: list) -> str:
    temp = 0
    dic = {}

    for part in participant:
        dic[hash(part)] = part
        print(part, int(hash(part)))
        temp += int(hash(part))
        print(temp)
    print()
    for com in completion:
        print(com, int(hash(com)))
        temp -= int(hash(com))
        print(temp)
    answer = dic[temp]
    return answer


participant = ["marina", "josipa", "nikola", "vinko", "filipa"]
completion = ["josipa", "filipa", "marina", "nikola"]

print(solution(participant, completion))