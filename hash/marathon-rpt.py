def solution(participant, completion):
    temp = 0
    dic = {}

    for part in participant:
        dic[hash(part)] = part
        temp += int(hash(part))

    for comp in completion:
        dic[hash(comp)] = comp
        temp -= int(hash(comp))

    answer = dic[temp]
    return answer


participant = ["leo", "kiki", "eden"]
completion = ["eden", "kiki"]
print(solution(participant, completion))
