def solution(a, b):
    answer = 0

    if a < b:
        for i in range(a, b+1):
            answer += i
    elif a > b:
        a, b = b, a
        for i in range(a, b+1):
            answer += i
    else:
        answer = a

    return answer



a, b = 3, 5
print(solution(a,b))