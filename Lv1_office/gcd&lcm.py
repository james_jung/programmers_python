import math

def gcd(n, m):
    result = math.gcd(n,m)
    return result


def lcm(n, m):
    result = n*m // gcd(n,m)
    return result

def solution(n, m):
    answer = [gcd(n,m),lcm(n,m)]
    return answer


n = 3
m = 12
print(solution(n,m))