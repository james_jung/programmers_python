def solution(n):
    n_list = list(str(n))
    n_list.sort(reverse=True)
    answer = ''.join(n_list)

    return int(answer)


n = 118372
print(solution(n))