def solution(s):
    answer = ''
    idx = 0

    for char in s:
        if char.isalpha():
            idx += 1
            if idx % 2 != 0:
                answer += char.upper()
            else:
                answer += char.lower()
        else:
            idx = 0
            answer += ' '
            continue

    return answer


s = "try hello world"
print(solution(s))