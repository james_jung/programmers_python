def solution(n):
    answer = 0
    conv = ''

    while n:
        conv += str(n%3)
        n //= 3

    multi = 1
    for i in reversed(conv):
        answer += int(i) * multi
        multi *= 3

    return answer


n = 125
print(solution(n))