def solution(s):
    answer = ''

    # 짝수이면
    n = int(len(s)//2)
    if len(s) % 2 == 0:
        answer = s[n-1:n+1]
    # 홀수이면
    else:
        answer = s[n]

    return answer


s = "qwer"
print(solution(s))