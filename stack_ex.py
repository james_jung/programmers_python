
def solution(s):
    answer = True
    stack = []

    for i in s:
        if i == '(':
            stack.append(i)
        elif i == ')':
            try:
                stack.pop()
            except:
                return False

    if len(stack) == 0:
        return True
    else:
        return False



s = "()()"
print(solution(s))