def solution(answers):
    first = [1,2,3,4,5]
    second = [2,1,2,3,2,4,2,5]
    third = [3,3,1,1,2,2,4,4,5,5]

    first_cnt = 0
    second_cnt = 0
    third_cnt = 0

    for index, answer in enumerate(answers):
        if answer == first[index%5]:
            first_cnt += 1
        if answer == second[index%8]:
            second_cnt += 1
        if answer == third[index % 10]:
            third_cnt += 1

    cnt_dict = {1:first_cnt, 2:second_cnt, 3:third_cnt}
    top_score = max(cnt_dict.values())
    result = [student for student, score in cnt_dict.itmes() if score==top_score]

    return result


answers = [1,2,3,4,5]
print(solution(answers))