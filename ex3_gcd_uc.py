# 유클리드 호제법 풀이
def gcd(a,b):
    if b > a:
        tmp = a
        a = b
        b = tmp

    while b > 0:
        c = b
        b = a % b
        a = c
    return a


print(gcd(192,72))
