

def solution(n, lost, reserve):
    net_reserver = set(reserve) - set(lost)
    net_lost = set(lost) - set(reserve)

    for i in net_reserver:
        if i-1 in net_lost:
            net_lost.remove(i-1)
        elif i+1 in net_lost:
            net_lost.remove(i+1)

    # 전체 - 최종적으로 체육복이 없는 사람 수
    answer = n - len(net_lost)
    return answer



n = 5
lost = [2,4]
reserve = [1,3,5]

print(solution(n, lost, reserve))