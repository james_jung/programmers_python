from math import gcd

def solution(n: int,m: int)->list:
    result = []

    # 최대공약수
    g = gcd(n, m)
    result.append(g)

    def lcm(x, y):
        return x * y // gcd(x,y)

    # 최소공배수
    l = lcm(n, m)
    result.append(l)

    return result


n = 3
m = 12

print(solution(n,m))