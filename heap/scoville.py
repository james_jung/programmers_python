import heapq

def solution(scoville: list, K: int) -> int:

    heapq.heapify(scoville)
    cnt = 0

    while True:
        if len(scoville) <= 1 and scoville[0] < K:
            cnt -= 1
            break
        if scoville[0] >= K:
            break

        new_scoville = heapq.heappop(scoville) + 2 * heapq.heappop(scoville)
        heapq.heappush(scoville, new_scoville)
        cnt += 1
        return cnt


scoville = [1, 2, 3, 9, 10, 12]
K = 7

print(solution(scoville, K))