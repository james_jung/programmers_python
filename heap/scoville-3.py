import heapq
import heapq

def solution(scoville, K):
    count = 0
    heapq.heapify(scoville)

    while True:
        if len(scoville) <= 1 and scoville[0] < K:
            count = -1
            break
        if scoville[0] >= K:
            break

        new_num = heapq.heappop(scoville) + (heapq.heappop(scoville) * 2)
        heapq.heappush(scoville, new_num)
        count += 1
    return count




scoville = [1, 2, 3, 9, 10, 12]
K = 7
print(solution(scoville, K))