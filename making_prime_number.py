from itertools import combinations


def prime_number(a, b, c):
    total = a + b + c
    for i in range(2, total):
        if total % i == 0:
            return False
    return True


def solution(nums):
    answer = 0
    a = list(combinations(nums, 3))
    for i in a:
        if prime_number(i[0],i[1],i[2]):
            answer += 1
    return answer



nums = [1,2,3,4]
print(solution(nums))


