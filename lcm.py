from math import gcd

def lcm(a, b):
    if a % b == 0: return b
    else: return lcm(b, (a%b))

def solution(arr):
    answer = 1
    for i in arr: answer = int((answer * i) / lcm(answer, i))
    return answer



arr = [2,6,8,14]
print(solution(arr))
