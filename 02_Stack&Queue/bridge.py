from collections import deque


def solution(bridge_length, weight, truck_weigths):
    cnt = 0
    on_bridge = deque([0 for _ in range(bridge_length)])

    while on_bridge:
        cnt += 1
        on_bridge.pop()
        if truck_weigths:
            if sum(on_bridge) + truck_weigths[0] <= weight:
                on_bridge.appendleft(truck_weigths.pop(0))
            else:
                on_bridge.appendleft(0)

    return cnt


bridge_length = 2
weight = 10
truck_weigths = [7,4,5,6]
print(solution(bridge_length, weight, truck_weigths))