from collections import deque

def solution(progresses, speeds):

    progresses = deque(progresses)
    speeds = deque(speeds)
    answer = []
    time = 0
    cnt = 0

    while len(progresses) > 0:
        if (progresses[0] + time*speeds[0]) >= 100:
            progresses.popleft()
            speeds.popleft()
            cnt += 1
        else:
            if cnt > 0:
                answer.append(cnt)
                cnt = 0
            time += 1
    answer.append(cnt)
    return answer


progresses = [93, 30, 55]
speeds = [1, 30, 5]
print(solution(progresses, speeds))