from collections import deque

def solution(priorities, location):
    answer = 0
    priorities = deque(priorities)


    while priorities:
        pop_num = priorities.popleft()
        max_val = max(priorities)

        if max_val == pop_num:
            answer += 1
            if location == 0:
                break
            else:
                location -= 1
        else:
            priorities.append(pop_num)
            if location == 0:
                location = len(priorities) - 1
            else:
                location -= 1

    return answer











    return answer







priorities = [2, 1, 3, 2]
location = 2
print(solution(priorities, location))