def solution(n, lost, reserve):
    net_reserve = set(reserve) - set(lost)
    net_lost = set(lost) - set(reserve)

    for i in net_reserve:
        if i-1 in net_lost:
            net_lost.remove(i-1)
        elif i+1 in net_lost:
            net_lost.remove(i+1)

    return n - len(net_lost)


n = 5
lost = [2,4]
reserve = [1,3,5]

print(solution(n, lost, reserve))