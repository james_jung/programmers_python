def solution(citations):
    citations.sort()
    l = len(citations)
    for i in range(l):
        if citations[i] >= l-i:
            return l-i
    return 0


# 3번 이상 인용된 논문이 3편



citations = [3, 0, 6, 1, 5]
print(solution(citations))