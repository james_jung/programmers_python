from collections import deque

def solution(bridge_length, weigth, truck_weights):

    cnt = 0
    q = deque([0 for _ in range(bridge_length)])

    while q:
        cnt += 1
        q.pop()
        if truck_weights:
            if sum(q) + truck_weights[0] <= weigth:
                q.appendleft(truck_weights.pop(0))
            else:
                q.appendleft(0)
    return cnt

bridge_length = 100
weight = 100
truck_weights = [10,10,10,10,10,10,10,10,10,10]

print(solution(bridge_length, weight, truck_weights))