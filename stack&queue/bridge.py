from collections import deque

def solution(bridge_length, weight, truck_weights):

    q = deque([0] * bridge_length)
    # q = deque([0 for _ in range(bridge_length)])
    cnt = 0

    while q:
        cnt += 1
        q.pop()
        if truck_weights:
            if sum(q) + truck_weights[0] <= weight:
                q.appendleft(truck_weights.pop(0))
            else:
                q.appendleft(0)
    return cnt

bridge_length = 2
weight = 10
truck_weights = [7,4,5,6]

print(solution(bridge_length, weight, truck_weights))