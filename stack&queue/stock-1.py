from collections import deque

def solution(prices):

    prices = deque(prices)
    answer = []

    while prices:
        c = prices.popleft()
        cnt = 0
        for i in prices:
            cnt += 1
            if c > i:
                break

        answer.append(cnt)

    return answer

prices = [1, 2, 3, 2, 3]
print(solution(prices))