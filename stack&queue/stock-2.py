from collections import deque

def solution(prices):

    prices = deque(prices)
    answer = []

    while prices:
        cnt = 0
        c = prices.popleft()

        for i in prices:
            cnt += 1
            if c > i:
                break

        answer.append(cnt)

    return answer


prices = [1, 2, 3, 2, 3]
print(solution(prices))