def solution(priorities, location):
    answer = 0
    while priorities!=[]:
        max_pri=max(priorities)
        pop_num=priorities.pop(0)
        if max_pri == pop_num:
            answer += 1
            if location == 0:
                break
            else:
                location -=1
        else:
            priorities.append(pop_num)
            if location==0:
                location=len(priorities)-1
            else:
                location-=1
    return answer